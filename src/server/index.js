var matches = require("../data/matches.json");
var deleveries = require("../data/deleveries.json");

var fs = require("fs");

const ipl = require("./ipl.js");

let toJasonPushHere = [];

let matchesPerYear = ipl.matchesPerYearFunc(matches);
let fileName="matchesPerYear"
convertToJson(matchesPerYear,fileName);

let matchesWonPerTeamPerYear = ipl.matchesWonPerTeamPerYearFunc(matches);
fileName="matchesWonPerTeamPerYear"
convertToJson(matchesWonPerTeamPerYear,fileName);

//let matchId2015=ipl.matchId2015(matches)
let year = "2016";
let matchId = ipl.matchId(matches, year);

let extraRunsPerTeam = ipl.extraRunsPerTeamFunc(deleveries, matchId);
fileName="extraRunsPerTeam"
convertToJson(extraRunsPerTeam,fileName);

year = "2015";
matchId = ipl.matchId(matches, year);
let bowlersEconomy = ipl.bowlersEconomyFunc(deleveries, matchId);
let top10Economy = ipl.top10EconomyFunc(bowlersEconomy);
fileName="top10Economy"
convertToJson(top10Economy,fileName);


function convertToJson(file,fileName) {
  var fileJson = JSON.stringify(file);
  fs.writeFile(`${fileName}.json`, fileJson, "utf8", err => {
    if (err) {
      console.error(err);
      return;
    }
    console.log("File has been created");
  });
}
