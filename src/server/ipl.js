//module.export{}

//1.function to return Number of matches played per year for all the years in IPL
// will return matchPerYear wich typeof object {"2008":58,"2009":57}

matchesPerYearFunc = matches =>
    matches.reduce((matchesPerYear, matches) => {
    //match.season is year
    //ifmatchesPerYear is undefined, means it have no value // {2017:   ,2016:   }
    if (!matchesPerYear[matches.season]) matchesPerYear[matches.season] = 0;
    matchesPerYear[matches.season]++;
    return matchesPerYear;
  }, {});

//2. function return Number of matches won of per team per year in IPL.
//{"2008":{"Kolkata Knight Riders":5,"Chennai Super Kings":9},}

matchesWonPerTeamPerYearFunc = matches =>
  matches.reduce((wonPerTeamPerYear, matches) => {
    if (!wonPerTeamPerYear[matches.season])
      wonPerTeamPerYear[matches.season] = {};
    else if (!wonPerTeamPerYear[matches.season][matches.winner])
      wonPerTeamPerYear[matches.season][matches.winner] = 0;
    wonPerTeamPerYear[matches.season][matches.winner]++;
    return wonPerTeamPerYear;
  }, {});

//3. return Extra runs conceded per team in 2016

//matchId stores id of year passed
matchId = (matches, year) =>
  matches.filter(matches => matches.season === year).map(matches => matches.id);

//Extra_run match_id is in deliveries.json
extraRunsPerTeamFunc = (deleveries, matchId) =>
  deleveries
    .filter(deleveries => matchId.includes(deleveries.match_id))
    .reduce((extraRunsPerTeam, deleveries) => {
      if (!extraRunsPerTeam[deleveries.bowling_team]) {
        extraRunsPerTeam[deleveries.bowling_team] = 0;
      }
      extraRunsPerTeam[deleveries.bowling_team] += parseInt(
        deleveries.extra_runs,0);
      return extraRunsPerTeam;
    }, {});

//4. return Top 10 economical bowlers in 2015

//bowlerEconomyData stores run conceded and total ball bowled
bowlersEconomyFunc = (deleveries, matchId) =>
  deleveries
    .filter(deleveries => matchId.includes(deleveries.match_id))
    .reduce((bowlersEconomyData, deleveries) => {
      if (!bowlersEconomyData[deleveries.bowler])
        bowlersEconomyData[deleveries.bowler] = [0, 0];

      bowlersEconomyData[deleveries.bowler][0] += parseInt(
        deleveries.total_runs);
      if (deleveries.wide_runs === "0" && deleveries.noball_runs === "0")
        bowlersEconomyData[deleveries.bowler][1]++;
      return bowlersEconomyData;
    }, {});


//return sorted player name according to =>{ total run conceded/total over}
top10EconomyFunc = bowlersEconomy => {
  let bowlers = Object.entries(bowlersEconomy)
  .reduce((economy, player) => {
    if (!economy[player[0]] && player[1][1] >= 12) economy[player[0]] = 0;
    if (player[1][1] >= 12)
      economy[player[0]] = player[1][0] / (player[1][1] / 6);
    return economy;
  }, {});

  top10Bowler = Object.entries(bowlers)
    .sort((a, b) => a[1] - b[1])
    .slice(0, 10);

  return top10Bowler;
};

//required modules exported
module.exports = {
  matchesPerYearFunc,
  matchesWonPerTeamPerYearFunc,
  matchId,
  extraRunsPerTeamFunc,
  bowlersEconomyFunc,
  top10EconomyFunc
};
