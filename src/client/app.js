//Function to fetch for all URL'S
async function getFetch(url) {
  let response = await fetch(url);
  let data = await response.json();
  return data;
}

// urls are passed to getFetch
let url = "../output/matchesPerYear.json";
//let chartType="Matches Per Year"
getFetch(url).then(data =>
  processingData_line(data, "matchesPYearContainer", "Matches Per Year")
);

url = "../output/extraRunsPerTeam.json";
//chartType="Extra Runs Per Team"
getFetch(url).then(data =>
  processingData_line(data, "extraRunsContainer", "Extra Runs Per Team")
);

url = "../output/top10Economy.json";
getFetch(url).then(data => {
  processingData_line(data, "top10EconomyContainer", "Economy per player");
});

url = "../output/matchesWonPerTeamPerYear.json";
getFetch(url).then(data =>
  processingData_Bar(data, "matchesWonPerTeamPerYearCS", "Matches won per year")
);

//Process to separate name and data to show on line chart

//This process is for Extra runs per year data and match per year data
let processingData_line = (fetchdFiles, containerName, chartType) => {
  let name = [];
  let data = [];
  // for (keys in fetchdFiles) {
  //   name.push(keys);
  //   data.push(fetchdFiles[keys]);
  // }
  name = Object.keys(fetchdFiles);
  data = Object.values(fetchdFiles);
  Charts(name, data, containerName, chartType);
};

//this process's is working on matches won per year by a team
//here the function is making the server data in form=>
// [{name:......,
//   data:...... }]

let processingData_Bar = (matchesWonPerYear, containerName, chartType) => {
  {
    let count = 0;
    let name = Object.keys(matchesWonPerYear);

    count = 0;

    //series is a object in which the High charts excepts the input for charts
    series = Object.values(matchesWonPerYear).reduce((series, team) =>
     {
      Object.entries(team).map(perTeam => 
        {
        if (!series[perTeam[0]])
          series[perTeam[0]] = new Array(name.length).fill(0);

        series[perTeam[0]][count] = perTeam[1];
      }),
        count++;
      return series;
    }, {});

    data = [];
    //console.log(series)
    for (teams in series) {
      data.push({ name: teams, data: series[teams] });
    }
    matchesWonPerYearSChart(name, data, containerName, chartType);
  }
};

//Function for line chart
Charts = (name, data, containerName, chartType) =>
  Highcharts.chart(containerName, {
    chart: {
      type: "line"
    },
    title: {
      text: null
    },

    subtitle: {
      text: "IPL"
    },
    xAxis: {
      categories: name
    },
    yAxis: {
      title: {
        text: chartType
      }
    },
    legend: {
      layout: "vertical",
      align: "right",
      verticalAlign: "middle"
    },

    series: [{ name: chartType, data: data }],
    responsive: {
      rules: [
        {
          condition: {
            maxWidth: 500
          },
          chartOptions: {
            legend: {
              layout: "horizontal",
              align: "center",
              verticalAlign: "bottom"
            }
          }
        }
      ]
    }
  });

//Stacked bar chart
matchesWonPerYearSChart = (year, data) =>
  Highcharts.chart("matchesWonPerTeamPerYearCS", {
    chart: {
      type: "column"
    },
    title: {
      text: null
    },
    subtitle: {
      text: "Matches won by teams per year"
    },
    xAxis: {
      categories: year,
      title: {
        text: "years"
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: "Matches played",
        align: "high"
      },
      labels: {
        overflow: "justify"
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat:
        '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.1f} won</b></td></tr>',
      footerFormat: "</table>",
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        //    stacking:'normal',
        pointPadding: 0.2,
        borderWidth: 0
      }
    },

    series: data
  });

/*
  //this process is for top economy player
let process2 = (top10Economy, container1, chartType) => {
  let name=[];
  let data = [];
  // for (keys in top10Economy) {
  //   name.push(top10Economy[keys][0]);
  //   data.push(top10Economy[keys][1])
  // }
  name=Object.keys(top10Economy)
  data=Object.values(top10Economy)
  Charts(name,data, container1, chartType);
};
*/
