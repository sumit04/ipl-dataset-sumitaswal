Part1: Transforming raw data of IPL to calculate the following stats:

1. Number of matches played per year for all the years in IPL.
2. Number of matches won of per team per year in IPL.
3. Extra runs conceded per team in 2016
4. Top 10 economical bowlers in 2015


Ball by ball details for all matches for all seasons.
https://www.kaggle.com/manasgarg/ipl/download


Part2: Used a static server and build a simple frontend app,where a HTTP request is made to get the json file and display the data as a visualization. 

The visualization is done using a library called highcharts.js.